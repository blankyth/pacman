﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PowerUp : Pill
{
    bool working = false;

	// Use this for initialization
	void Start ()
    {
        StartCoroutine(Animate());
	}

    IEnumerator Animate()
    {
        float waitTime = 0.05f;
        working = true;

        while (working == true)
        {
            transform.localScale = new Vector3(0.3f, 0.3f, 0.3f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.8f, 0.8f, 0.8f);
            yield return new WaitForSeconds(waitTime);
            transform.localScale = new Vector3(0.5f, 0.5f, 0.5f);
            yield return new WaitForSeconds(waitTime);

            //Debug.Log("ANIMATION IN PROGRESS");
        }
    }

    // Update is called once per frame
    void Update ()
    {
		
	}

    
}
