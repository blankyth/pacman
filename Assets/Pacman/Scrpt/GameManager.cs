﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    AudioSource _audioSource;

    [SerializeField]
    AudioClip _audioStart;

    [SerializeField]
    PacmanMovement _pacman;

    [SerializeField]
    GameObject _gameOverUI;

    bool _isGameReady;

    public bool IsGameReady()
    {
        return _isGameReady;
    }

    private void Awake()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start()
    {

        StartCoroutine(CheckStartMusic());

        if (_pacman == null)
        {
            throw new UnityException("Pacman is missing!!");
        }

        _gameOverUI.SetActive(false);
    }

    // Update is called once per frame
    void Update()
    {
        if (CheckDefeat())
        {
            OnDefeat();
        }
        else if (CheckVictory())
        {
            OnVictory();
        }
    }

    private void OnDefeat()
    {
        _gameOverUI.SetActive(true);
        _isGameReady = false;
    }

    private void OnVictory()
    {
        Debug.Log("TROPPO FORTE ZIO!!");
        //SceneManager.LoadScene("pacman2");
    }



    /// <summary>
    /// Controlla le condizioni di vittoria.
    /// </summary>
    /// <returns>True se il giocatore ha vinto, altrimenti false.</returns>
    bool CheckVictory()
    {
        return (_pacman.GetEatenPills() == Pill.GetTotalPillCount());
    }


    /// <summary>
    /// Controlla la condizione di sconfitta quando Pacman arriva a 0 vite.
    /// </summary>
    /// <returns></returns>
    bool CheckDefeat()
    {
        return (_pacman.GetLives() == 0);
    }

    /// <summary>
    /// Aspetta che la musica di inizio sia terminata prima di permettere l'avvio del gioco.
    /// </summary>
    /// <returns></returns>
    IEnumerator CheckStartMusic()
    {
        _isGameReady = false;
        _audioSource.PlayOneShot(_audioStart);

        while (_audioSource.isPlaying)
            yield return null;
        _isGameReady = true;
    }

    public void RestartLevel()
    {
        SceneManager.LoadScene("Pacman");
    }
}
