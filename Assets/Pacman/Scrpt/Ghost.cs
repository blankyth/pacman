﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Ghost : MonoBehaviour
{

    NavMeshAgent _navAgent;
    GameObject _player;
    PacmanMovement _pacman;
    GameManager _gameManager;
    Renderer _ghostMeshRenderer;

    [SerializeField]
    GameObject ghostMesh;

    [SerializeField]
    GameObject eyesMesh;

    [SerializeField]
    Transform spawnPoint;

    [SerializeField]
    Material AfraidMaterial;

    [SerializeField]
    Material defaultMaterial;

    bool _isDead;

    Collider _collider;

    static public int Points = 200;

    private void Awake()
    {
        _gameManager = GameObject.FindObjectOfType<GameManager>();
        _collider = gameObject.GetComponent<Collider>();
        _navAgent = GetComponent<NavMeshAgent>();

        _ghostMeshRenderer = ghostMesh.GetComponent<Renderer>();
        defaultMaterial = _ghostMeshRenderer.material;
    }

    //private void OnDisable()
    //{
    //    Destroy(ghostMesh);
    //}

    // Use this for initialization
    void Start()
    {
        _player = GameObject.FindGameObjectWithTag("Player");

        _pacman = _player.GetComponent<PacmanMovement>();
        eyesMesh.SetActive(false);

        //Debug.Log("TOTALE PILLOLE=" + Pill.GetTotalPillCount());

        if (spawnPoint == null)
            throw new UnityException("Spawn point is missing.");
    }

    // Update is called once per frame
    void Update()
    {
        if (_gameManager.IsGameReady() == false)
            return;
        ghostMesh.transform.position = transform.position;
        eyesMesh.transform.position = transform.position;

        if (!_isDead) // Ghost alive
        {
            _navAgent.SetDestination(_player.transform.position);
            SetMaterial();
        }
        else // Ghost dead
        {
            //if((gameObject.transform.position-spawnPoint.transform.position).magnitude <= 0.5)
            if(IsInsideSpawn())
            {
                // Respawn
                Respawn();
            }
        }
    }

    private void Respawn()
    {
        ghostMesh.SetActive(true);
        eyesMesh.SetActive(false);
        _isDead = false;
        _collider.enabled = true;

        SetMaterial();
    }

    private void SetMaterial()
    {
        if (_pacman.HasPowerUp())
            _ghostMeshRenderer.material = AfraidMaterial;
        else
            _ghostMeshRenderer.material = defaultMaterial;
    }

    bool IsInsideSpawn()
    {
        return (gameObject.transform.position - spawnPoint.transform.position).magnitude <= 0.5;
    }

    public void OnDeath()
    {
        ghostMesh.SetActive(false);
        eyesMesh.SetActive(true);
        _isDead = true;
        _navAgent.SetDestination(spawnPoint.position);
        _collider.enabled = false;
    }
}
