﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PacmanMovement : MonoBehaviour
{

    GameManager _gameManager;

    [SerializeField]
    float MovementSpeed;

    public int TotalPoints;

    int Lives = 3;

    public int GetLives()
    {
        return Lives;
    }

    AudioSource _audioSource;

    [SerializeField]
    AudioClip _audioMove;

    [SerializeField]
    AudioClip _audioDeath;

    bool _hasPowerUp;

    public bool HasPowerUp()
    {
        return _hasPowerUp;
    }

    /// <summary>
    /// Tempo trascorso da quando si è raccolto  il Power-Up.
    /// </summary>
    float _PowerUpElapsedTime = 0;

    /// <summary>
    /// Indica la durata dell'ultimo Power-Up raccolto.
    /// </summary>
    float _PowerUpDuration = 10;

    /// <summary>
    /// Rappresenta il numero di fantasmi mangiati durante l'effetto del Power-Up.
    /// </summary>
    int _eatenGhost;

    /// <summary>
    /// Rappresenta il numero di pillole mangiate da Pacman.
    /// </summary>
    int _eatenPills;

    public int GetEatenPills()
    {
        return _eatenPills;
    }

    private void Awake()
    {
        _gameManager = GameObject.FindObjectOfType<GameManager>();
        _audioSource = GetComponent<AudioSource>();
    }

    // Use this for initialization
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {

        if (_gameManager.IsGameReady() == false)
            return;

        float h = Input.GetAxis("Horizontal");
        float v = Input.GetAxis("Vertical");
        //Debug.Log("h: " + h + " - v: " + v);
        if (h != 0)
        {
            transform.Translate(Vector3.right * h * MovementSpeed);
        }
        else
        {
            transform.Translate(Vector3.forward * v * MovementSpeed);
        }

        if (h != 0 || v != 0)
        {
            if (!_audioSource.isPlaying)
                _audioSource.PlayOneShot(_audioMove);
        }

        if (_hasPowerUp)
        {
            _PowerUpElapsedTime += Time.deltaTime;
            //Debug.Log("ELAPSED TIME " + _PowerUpElapsedTime);
            if (_PowerUpElapsedTime >= _PowerUpDuration)
            {
                _hasPowerUp = false;
                _eatenGhost = 0;
            }
        }
    }

    void OnTriggerEnter(Collider other)
    {


        if (other.gameObject.tag == "pill")
        {
            OnEatPill(other);
        }

        if (other.gameObject.tag == "ghost")
        {
            if (!_hasPowerUp)
                OnHit();
            else
            {
                OnEatGhost(other);
            }
        }
    }

    /// <summary>
    /// Codice eseguito quando Pacman mangia un fantasma.
    /// </summary>
    /// <param name="other"></param>
    private void OnEatGhost(Collider other)
    {
        _eatenGhost++;

        TotalPoints += (int)Mathf.Pow(2, _eatenGhost - 1) * Ghost.Points;
        //Destroy(other.gameObject);
        other.gameObject.SendMessage("OnDeath", SendMessageOptions.RequireReceiver);
    }

    /// <summary>
    /// Codice eseguito quando Pacman viene colpito da un fantasma.
    /// </summary>
    void OnHit()
    {
        Debug.Log("SEI MORTO!!!");

        if (!_audioSource.isPlaying)
        {
            _audioSource.Stop();
        }
        _audioSource.PlayOneShot(_audioDeath);

        Lives -= 1;
    }

    /// <summary>
    /// Codice eseguito quando si mangia una pilola.
    /// </summary>
    /// <param name="other"></param>
    void OnEatPill(Collider other)
    {
        Debug.Log("GNAM!!");

        _eatenPills++;

        Pill pill = other.gameObject.GetComponent<Pill>();
        TotalPoints += pill.Points;

        if (pill is PowerUp)
        {
            _hasPowerUp = true;
            _PowerUpElapsedTime = 0;
            Debug.Log("POWER UP!!");
        }

        Destroy(other.gameObject);

        /*if (_eatenPills == Pill.GetTotalPillCount())
        {
            Debug.Log("HAI VINTO!!!!111!!11!!");
        }*/
    }
}
